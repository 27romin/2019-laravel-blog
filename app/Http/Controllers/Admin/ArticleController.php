<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:administrator');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderBy('updated_at', 'desc')
        ->with('user')
        ->paginate(10);
        
        return view('admin.articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            'title' => 'required',
            'slug' => 'required|alpha_dash|unique:articles',
            'body' => 'required',
        ]);

        request()->request->add([
            'user_id' => auth()->user()->id
        ]);

        Article::create(request()->all());
        return redirect()->route('admin.articles.index')->withSuccess('Article Created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Article $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        return view('admin.articles.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Article $article
     * @return \Illuminate\Http\Response
     */
    public function update(Article $article)
    {
        request()->validate([
            'title' => 'required',
            'slug' => 'required|alpha_dash|unique:articles,slug,'.$article->id,
            'body' => 'required',
        ]);

        $article->update(request()->all());
        return redirect()->route('admin.articles.index')->withSuccess('Article Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Article $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return redirect()->route('admin.articles.index')->withSuccess('Article Deleted.');
    }
}
