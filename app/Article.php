<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
    	'title', 'slug', 'body', 'user_id'
    ];

    /**
     * Use slug instead of id.
     */
    public function getRouteKeyName() 
    {
        return 'slug';
    }

    /**
     * An article is created by an user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
