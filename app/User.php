<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * A user can have many roles.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Check multiple roles.
     *
     * @param array $roles
     */
    public function hasRoles(array $roles)
    {
        return $this->roles->whereIn('name', $roles)->first();
    }

    /**
     * Check user role.
     *
     * @param $role
     */
    public function role($role)
    {
        return $this->roles->where('name', $role)->first();
    }

    /**
     * Check if user is an administrator.
     */
    public function getAdministratorAttribute()
    {
        return $this->role('administrator');
    }

    /**
     * A user can have many articles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles()
    {
        return $this->hasMany(Article::class, 'user_id', 'id');
    }
}
