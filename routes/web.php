<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
	return view('welcome');
});

Route::namespace('Admin')->prefix('admin')->group(function () {
    Route::get('home', 'HomeController@index')->name('admin.home.index');
    Route::resource('articles', 'ArticleController', ['as' => 'admin'])->except(['show']);
});

Route::namespace('App')->prefix('home')->group(function () {
    Route::get('', 'HomeController@index')->name('app.home.index');
    Route::resource('articles', 'ArticleController', ['as' => 'app'])->only(['index', 'show']);
	Auth::routes();
});