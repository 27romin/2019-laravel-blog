@extends('admin.templates.default')

@section('navigation')
    @include('admin.templates.partials.navigation')
@endsection

@section('content')
@if ($articles->hasMorePages() or $articles->lastItem())
	{{ $articles->links() }}
@endif
	<h2>Articles</h2>
	@include('admin.templates.partials.messages.success')
	<table>
	    <thead>
	      <tr>
	        <th>ID</th>
	        <th>Title</th>
	        <th>By</th>
	        <th>Actions</th>
	      </tr>
	    </thead>
	    <tbody>
	       @forelse($articles as $article)
					<tr>
			        <td>{{ $article->id }}</td>
			        <td>{{ $article->title }}</td>
			        <td>{{ ucfirst($article->user->name) }}</td>
			        <td>
			        	<a href="{{ route('admin.articles.edit', $article->slug) }}">edit</a>
			        </td>
			        <td>
			        	<form action="{{ route('admin.articles.destroy', $article->slug) }}" method="POST">
			        		{{ method_field('DELETE') }}
						    {{ csrf_field() }}
						    <button type="submit">delete</button>
						</form>
			        </td>
	      		</tr>
    		@empty
    			<tr>
    				<td colspan="4">
    					No articles posted.
    				</td>
    			</tr>
    		@endforelse
	    </tbody>
		</table>
@endsection
