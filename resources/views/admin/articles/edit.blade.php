@extends('admin.templates.default')

@section('navigation')
    @include('admin.templates.partials.navigation')
@endsection

@section('content')
    <h2>Edit Article</h2>
	@include('admin.templates.partials.messages.errors')
	<form action="{{ route('admin.articles.update', $article->slug) }}" method="POST">
		{{ csrf_field() }}
        {{ method_field('PATCH') }}
		<fieldset>
			<legend>Title</legend>
			<input name="title" type="text" value="{{ $article->title ? $article->title : '' }}">
		</fieldset>
		<fieldset>
			<legend>Slug</legend>
			<input name="slug" type="text" value="{{ $article->slug ? $article->slug : '' }}">
		</fieldset>
		<fieldset>
			<legend>Body</legend>
			<textarea name="body" cols="30" rows="10">{{ $article->body ? $article->body : '' }}</textarea>
		</fieldset>
		<button type="submit">Submit</button>
	</form>
@endsection
