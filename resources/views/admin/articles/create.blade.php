@extends('admin.templates.default')

@section('navigation')
    @include('admin.templates.partials.navigation')
@endsection

@section('content')
    <h2>Create Article</h2>
	@include('admin.templates.partials.messages.errors')
	<form action="{{ route('admin.articles.store') }}" method="POST">
		{{ csrf_field() }}
		<fieldset>
			<legend>Title</legend>
			<input name="title" type="text" value="{{ old('title') ? old('title') : '' }}">
		</fieldset>
		<fieldset>
			<legend>Slug</legend>
			<input name="slug" type="text" value="{{ old('slug') ? old('slug') : '' }}">
		</fieldset>
		<fieldset>
			<legend>Body</legend>
			<textarea name="body" cols="30" rows="10">{{ old('body') ? old('body') : '' }}</textarea>
		</fieldset>
		<button type="submit">Submit</button>
	</form>
@endsection