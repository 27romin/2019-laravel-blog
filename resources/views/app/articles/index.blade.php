@extends('app.templates.default')

@section('navigation')
    @include('app.templates.partials.navigation')
@endsection

@section('content')
    <div class="col-md-9">
        @if ($articles->hasMorePages() or $articles->lastItem())
            {{ $articles->links() }}
        @endif

        @foreach($articles as $article)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{ route('app.articles.show', $article->slug) }}">{{ $article->title }}</a>
                </div>
                <div class="panel-body">
                    {!! $article->body !!}
                </div>
                <div class="panel-footer">
                    Created By: {{ ucfirst($article->user->name) }}
                </div>
            </div>
        @endforeach
    </div>
@endsection
