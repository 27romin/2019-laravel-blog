@extends('app.templates.default')

@section('navigation')
    @include('app.templates.partials.navigation')
@endsection

@section('content')
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">{{ $article->title }}</div>
            <div class="panel-body">
                {!! $article->body !!}
            </div>
            <div class="panel-footer">
                Created By: {{ ucfirst($article->user->name) }}
            </div>
        </div>
    </div>
@endsection
